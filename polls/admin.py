from django.contrib import admin
from polls.models import Poll, Choice

class ChoiceInline(admin.TabularInline):
	model = Choice
	extra = 3

class PollAdmin(admin.ModelAdmin):
	fields = ['question', 'author', 'pub_date'] #change object
	list_display = ('question', 'author', 'pub_date',) #list of objects
	list_filter = ['pub_date']
	search_fields = ['question']
	date_hierarchy = 'pub_date'

class ChoiceAdmin(admin.ModelAdmin):
	list_display = ('answer', 'poll', 'count',)



admin.site.register(Poll, PollAdmin)
admin.site.register(Choice, ChoiceAdmin)

